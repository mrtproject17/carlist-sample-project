package com.taksim.cars

import android.app.Application
import com.taksim.cars.module.mainModule
import com.taksim.cars.module.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class CarsApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@CarsApplication)
            modules(listOf( mainModule,networkModule))
        }


    }
}