package com.taksim.cars.data.model.response

import com.google.gson.annotations.SerializedName

data class Vehicles (

	@SerializedName("ID") val iD : Int,
	@SerializedName("FirstRegistration") val firstRegistration : String,
	@SerializedName("AccidentFree") val accidentFree : Boolean,
	@SerializedName("Images") val images : List<String>,
	@SerializedName("PowerKW") val powerKW : Int,
	@SerializedName("Address") val address : String,
	@SerializedName("Price") val price : Int,
	@SerializedName("Mileage") val mileage : Int,
	@SerializedName("Make") val make : String,
	@SerializedName("FuelType") val fuelType : String
)