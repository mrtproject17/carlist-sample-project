package com.taksim.cars.data.model.response

import com.google.gson.annotations.SerializedName


data class CarsResponse (
	@SerializedName("vehicles") val vehicles : List<Vehicles>
)