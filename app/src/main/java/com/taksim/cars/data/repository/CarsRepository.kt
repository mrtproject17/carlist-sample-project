package com.taksim.cars.data.repository

import android.util.Log
import com.taksim.cars.data.model.response.ApiError
import com.taksim.cars.data.model.response.CarsResponse
import com.taksim.cars.network.RestInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarsRepository(val restInterface: RestInterface){

    inline fun getCars(
                         crossinline successHandler: (CarsResponse) -> Unit,
                         crossinline failureHandler: (Throwable?) -> Unit,
                         crossinline errorHandler: (ApiError) -> Unit) {
        restInterface.getCars().enqueue(object : Callback<CarsResponse> {
            override fun onResponse(call: Call<CarsResponse>?, response: Response<CarsResponse>?) {
                response?.let {
                    if (it.isSuccessful) {
                            successHandler(response.body())
                    } else {

                        val error= ApiError("","","")
                        errorHandler(error)
                    }
                }

            }
            override fun onFailure(call: Call<CarsResponse>?, t: Throwable?) {
                failureHandler(t)
            }
        })
    }

}