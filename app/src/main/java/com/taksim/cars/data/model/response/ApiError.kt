package com.taksim.cars.data.model.response

import com.google.gson.annotations.SerializedName

data class ApiError(@SerializedName("errorMessage") var errorMessage: String?,
                    @SerializedName("errorCode") var errorCode: String?,
                    @SerializedName("errorData") var errorData: String?

)