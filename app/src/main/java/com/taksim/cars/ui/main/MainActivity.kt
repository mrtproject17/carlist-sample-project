package com.taksim.cars.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.taksim.cars.R
import com.taksim.cars.data.model.response.CarsResponse
import com.taksim.cars.data.model.response.Vehicles
import com.taksim.cars.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() , SwipeRefreshLayout.OnRefreshListener  {
    override fun onRefresh() {
        binding?.swipeLayout?.isRefreshing = false
        binding?.isLoading=true
        viewModel.getCars()
    }


    val viewModel: MainViewModel by viewModel()
    private var binding: ActivityMainBinding? = null
    private lateinit var carListAdapter: CarListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding?.isLoading=true
        viewModel.getCars()
        observe()
        binding?.swipeLayout?.setOnRefreshListener(this)
    }

    private fun observe() {
        viewModel.apiResponse.observe(this, Observer {
            binding?.isLoading=false
            if(it!=null){
                    loadCars(it)

            }
        })

    }

    private fun loadCars(carResponse:CarsResponse) {
        carListAdapter = CarListAdapter(carResponse.vehicles,object: CarListAdapter.OnItemClickListener{
                override fun onItemClick(item: Vehicles) {

                }

            })
            binding?.listRecycler?.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            binding?.listRecycler?.adapter = carListAdapter
            carListAdapter.notifyDataSetChanged()


    }

}
