package com.taksim.cars.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso

import com.taksim.cars.data.model.response.Vehicles
import com.taksim.cars.databinding.RowCarDesignBinding


class CarListAdapter(private var cars: List<Vehicles>,private val listener:OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<CarListAdapter.TripItemViewHolder>() {
    override fun onBindViewHolder(holder: TripItemViewHolder, position: Int) {
        return holder.bind(cars[position],listener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripItemViewHolder = TripItemViewHolder.create(parent)


//    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ContactItemViewHolder = ContactItemViewHolder.create(parent)

//    override fun onBindViewHolder(holder: ContactItemViewHolder?, position: Int) {
//        return holder?.bind(contactList[position])!!
//    }


    override fun getItemCount() = cars.count()


    class TripItemViewHolder(private var binding: RowCarDesignBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(
            car: Vehicles,
            listener: OnItemClickListener
        ) = with(binding) {
            binding.car=car

            Picasso.with(itemView.context).load(car.images.get(0)).into(binding.image)
            itemView.setOnClickListener {
                car?.let { it1 ->
                    listener.onItemClick(it1) }
            }


        }

        companion object {
            fun create(parent: ViewGroup?):TripItemViewHolder {
                val binding = RowCarDesignBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
                return TripItemViewHolder(binding)
            }
        }
    }


    interface OnItemClickListener {
        fun onItemClick(item: Vehicles)
    }

}