package com.taksim.cars.ui.main

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.taksim.cars.data.model.response.CarsResponse
import com.taksim.cars.data.repository.CarsRepository
import android.R
import android.widget.ImageView
import com.squareup.picasso.Picasso
import androidx.databinding.BindingAdapter



class MainViewModel(val repo : CarsRepository,val pref : SharedPreferences) : ViewModel() {

    val apiResponse = MutableLiveData<CarsResponse>()

    fun getCars(){
        repo.getCars(
            {
                    apiResponse.value=it
            },{
                apiResponse.value=null
            },{
                apiResponse.value=null
            })

    }

    @BindingAdapter("bind:imageUrl")
    fun loadImage(view: ImageView, imageUrl: String) {
        Picasso.with(view.getContext())
            .load(imageUrl)
            .placeholder(R.drawable.picture_frame)
            .into(view)
    }

}