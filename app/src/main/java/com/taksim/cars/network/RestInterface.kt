package com.taksim.cars.network

import com.taksim.cars.data.model.response.CarsResponse
import retrofit2.Call
import retrofit2.http.*

interface RestInterface {


  @GET("iOS-TechChallange/api/index/make=all.json")
    fun getCars(): Call<CarsResponse>


}

